package ru.petshop.demo.mapper;

import java.util.List;

import ru.petshop.demo.entity.BookEntity;

public interface BookMapper {

    List<BookEntity> getAll();

    BookEntity getOne(Long id);

    void insert(BookEntity book);

    void update(BookEntity book);

    void delete(BookEntity book);

}
