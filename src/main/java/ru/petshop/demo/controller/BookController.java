package ru.petshop.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import ru.petshop.demo.entity.BookEntity;
import ru.petshop.demo.mapper.BookMapper;

@RestController
public class BookController {

    @Autowired
    private BookMapper bookMapper;

    @RequestMapping("/book/getBooks")
    public List<BookEntity> getBooks() {
        List<BookEntity> books=bookMapper.getAll();
        return books;
    }

    @RequestMapping("/book/getBook")
    public BookEntity getBook(Long id) {
        BookEntity book=bookMapper.getOne(id);
        return book;
    }

    @RequestMapping("/book/add")
    public void save(@RequestBody BookEntity book) {
        bookMapper.insert(book);
    }

    @RequestMapping(value="/book/update")
    public void update(@RequestBody BookEntity book) {
        bookMapper.update(book);
    }

    @RequestMapping(value="/book/delete")
    public void delete(@RequestBody BookEntity book) {
        bookMapper.delete(book);
    }


}