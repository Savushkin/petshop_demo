package ru.petshop.demo.entity;

public class BookEntity {

    private Integer id;
    private String title;
    private String author;
    private Double price;
    private Integer qty;

    public BookEntity() {
        super();
    }

    public BookEntity(String title, String author, Double price) {
        super();
        this.title = title;
        this.author = author;
        this.price = price;
    }

    public BookEntity(String title, String author, Double price, Integer qty) {
        super();
        this.title = title;
        this.author = author;
        this.price = price;
        this.qty = qty;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @Override
    public String toString() {
        return "BookEntity{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                ", qty=" + qty +
                '}';
    }
}
